# Blacwell's Webstead

[![Netlify Status](https://api.netlify.com/api/v1/badges/c3fb43a9-83da-4142-acb2-e5b2bf6d7780/deploy-status)](https://app.netlify.com/sites/blacwell/deploys)

> By podcast, article, or film; we, at Blacwell, shall kindle your soul with the wonderful traditions and lore inherited from our European forefathers.
