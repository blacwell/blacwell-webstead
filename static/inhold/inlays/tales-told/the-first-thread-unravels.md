---
title: The First Thread Unravels
author: Alder Blæcwell
lead: 'Reflecting on the introductory episode of Thread Ends, and talking a little more about Blacwell, and my (Alder) motivations for beginning this work. There is also the clue for the—admittedly hard—riddle.'
uttered: 1649437200000
edited:
audio: https://cdn.blacwell.com/tt--0100e--spoken.m4a
tags:
  - thread-ends
---

Due to the nature of _Thread Ends_—is without a script—I shall not be providing a full writ for these episodes, althought, if I feel anything is of particular import, or is better illustrateed in article form, I shall include it; the forthnightly riddles, I shall include here, as it is such content: one may have to re-read each line several times to gain a greater understanding.

If one is particually interested in reading my motivation and the story behind the name 'Blacwell', one can be find it on the about page.

## Riddle

_Near the dead they teem | with earth-bound tusks, twain_
Where did some Northmen consider Hel to be, and what near that location is two tusked?

_their name in middle tongue | it, too, does describe me well_
This beings was differently named—by some—in the middle language of that which one knows I speak. In my modern tongue, this creatur is no longer named as such, and the name now describes a type of fish.

_across the whale-way, we did ride | welcomed, then rebuked_
From where the whales make their journey, that is what was crossed. Originally invited, but soon the welcomer would come to feel regret his plea.

_we found a home for ourselves | a healthy legacy, did long follow._
Despite the unwelcoming, they did not leave, and ended up the better for it.
