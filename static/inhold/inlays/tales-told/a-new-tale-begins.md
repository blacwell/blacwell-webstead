---
title: A New Tale Begins
author: Alder Blæcwell
lead: 'We have been left such a wonderful hoard of tales from those who came before, but many of us take for granted such a valuable inheritance, and worse yet, many do not even know of its existence. Unlike the beautiful cathedrals that still stand tall, ever-reminding us of the greatness of those who build them, our stories are another matter; more important than any building, yet etherial and delicate, this folklore deserves to be remembered. If you are of a like-minded fellow, come, join me by the hearth, and together we shall explore our heritage, one tale at a time.'
uttered: 1648851360000
edited:
audio: https://cdn.blacwell.com/tt--u000--spoken--endly.m4a
tags:
  - test tag 1
  - test tag 2
  - test tag 3
---

## About

This episode, the first of, (hopefully) many, will serve as an introduction to the _Tales Told_ series, and _Blacwell_, rather than delving into any particular tale, although one could call this the beginning of a tale.
With so much vying for our attention, I think it is useful to first lay out our ambitions and plans, so you know whether this series is worth your time. Equally important as where we are going, is where we are from, and the position in which we view the world. I feel it is best to be forthright, because as with many things at the moment, one is rarely able to share a word of dissent without it being another's bane (such is the weak-hearted nature of modern man), and the topic of our tradition is no exception. Two starkly opposing view have taken shape, creating a gap so wide, one would be forgiven for lacking the ability to make head or tails of it. Mutated into a horrible visage by outside interests, it has become hard to find fact from fiction and unearth a reasoned position. Because of this, we shall share our thoughts openly and allow you to be the judge of our value.
At Blacwell, we are a very traditionally minded sort. We look to the tales of our past with pride and respect, and find a comfort and guidance in knowing them. _Tales Told_ exists to merrymake our tradition and share in the wisdom it brings, but even so, if something is of a disagreeable nature to our wise, we shall no shy away from delving into it; all things contain knowledge and offer us a chance to learn.
To be clear, do not expect us to sanitize the tales we tell for a modern, progressive audience; nor, becry a lack of racial or sexual diversity; nor, provide trigger-warning for the weak-hearted. We do not see ourselves as better than our forefathers, and won't pontificate modern ideals as thought they were infallible.

### Tales Told

But enough rambling; let's get to the topic at hand, _Tales Told_. Every fortnight (on Friday) we shall dedicate about 20 minutes to a particular tale:

1. First, we shall talk about its context: where & when it originates, who wrote it, the surrounding culture, wild tangents about hardly related topic, &c;
2. then, a summarized reading of the tale;
3. followed by an analysis of piece, what it represents, how it is related in theme to other works, &c.;
4. and finally, a summary of the most important bits, and with a little riddle for the topic of the next episode.

With that said, I suspect you want to know what I mean by 'tale'. There has been lots of talk about tradition and heritage, but how does that relate to the particular idea of a 'tale'. I'm leaving definition rather open, with the focus being, more so, on the function of the tale, rather than that of itself; anything, from fairytales, to chronicles, to nursery rhymes; whether fact or fiction, prose or poetry, or anything in between. Who knows, if this continues for a while, and I run out of ideas, I might try reading narratives from the stars. The key concept that ties it all together, is the narrative's ability to shed a light on our forefathers, and the ways they conceived of life.

One thing to keep in mind: it is European tales which will be the focus of this programme. It is not to say I won't reference other culture, outside of Europe, from time-to-time, but the focus will remain on Europe and **our** heritage.

#### Thread Ends

A fortnight is a long time, so, for those who want more, there will be a smaller, 'bonus', episode released on the Fridays in between main episodes. Called 'Thread Ends', it will be a time for me to answer questions, bring up some of the cut content from the previous uttering, share news & updates, and just have a calmer and more relaxed conversation. If that sound appealing, the first episode will be next week.

#### Fellowship

A foundational aspect of Tales Told, and Blacwell in general, is the focus of fellowship. I'm never going to be the most knowledgeable person on any particular tale, especially those from outside England, so in order to produce the most valuable and insightful work, the help of like-minded fellows will be indispensable.
If you have any ideas about a tale to cover, have an insight into a present or future topic, want to gloat about solving a previous riddle; or, just want to say 'hello', then you can head to the webstead (blacwell.com) where you can find links to join the conversation.

### Blacwell

That is all I shall say about Tales Told; there is nothing more but to show you the work. But, before I end, I would like to speak a bit more on 'Blacwell', the brand behind the work. One may wonder why I have chosen to publish this work through a brand, rather than just using my own name, since I am only a single person, at present. Well, I'm a big dreamer. It is the case that even before finishing writing this episode, I'd several other concepts ready to start. I not quite the fool to begin them all at once, but once a rhythm is set with Tales Told I shall supplement the output with the different works, so look forward to that. I'm conscious that if I continue on, this will become one great ramble. So, I shall leave it here, with you left in wonder for what will come next.

### Afterword

The first Thread Ends will be published next Friday (April 8) and I will talk a little more about my motivations, the story behind the naming of 'Blacwell' & 'Tales Told', and give a clue to the answer of the riddle (for those in need).
One can reach me by email (alder@blacwell.com), or join the conversation (blacwell.com/chat). Written versions and other interesting things at the webstead (blacwell.com).
Until we meet again, farewell.

_THE RIDDLE, PART 1_

Near the dead they teem | with earth-bound tusks, twain
their name in middle tongue | it, too, does describe me well
across the whale-way, we did ride | welcomed, then rebuked
we found a home for ourselves | a healthy legacy, did long follow.

## Credits

Music:
Vindsvept - Nödår (https://vindsvept.bandcamp.com)

Sound effects:
Joseph Sardin - Fireplace 1, Fireplace 2 (https://bigsoundbank.com/)
