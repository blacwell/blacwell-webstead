---
title: About Us
author: Alder Blæcwell
lead: 'We have been left such a wonderful hoard of tales from those who came before, but many of us take for granted such a valuable inheritance, and worse yet, many do not even know of its existence.'
uttered: 1649437200000
edited:
tags:
  - blacwell
---

## The Work

Main goal of Blacwell is to produce creative and informative works designed to enthuse and educate people on the wonderful heritage of our European forefathers.

To that end, we produce works in a range of formats; spoken, written, and shown. Currently, we have a podcast series called _Tales Told_, with written articles that go alongside each episode, where, once a fortnight, we talk about a particular tale from European tradition, and use it as a jumping off point to talk about related ideas.

## The Motivation

I feel it is important to lay out what we seek to achieve with our works, as so many men report to share in a similar passion for our traditions, but are motivated by a devious kind; they salt the earth with falsehoods, besmirch our forefathers, and pollute the minds of our children, all while claiming they are rightwise. We shall be forthright with our nature, so one can decide for oneself the value of our endeavour.

_Put simply:_
**At Blacwell, we are a very traditionally minded sort.**

We have a loving respect for our tradition and for those who shaped it. Our goal with Blacwell it to share this lore with all good folk that may listen, in hopes that all our lives will be made better.

To be clear, do not expect us to craft our works for the modern, progressive audience, nor becry 'a lack of diversity', nor pontificate ideals of morality in an attempt to place ourselves above our forefather. We are here to merrymake our forefathers and learn from their lessons.

## The Name

I, Alder of Blacwell, have developed a crippling addiction to English purism (opposition to foreign influence on the lovely English language), such that, when required to name this endeavour, I could not bring myself to use anything outside the native stock of words.

With that, the name is much a calque (word-for-word translation) of the modern English 'inkwell'; originally, I was going to use 'inkhorn', as it is the term used in Old English, but I discovered it had a modern definition, which, funnily enough, relates to the topic at hand.

"But, why 'inkwell'?", I here one ask; well, I rather enjoy the metaphorical, and actual, sense of the term: 'ink' is a substance of creation, that which life is moulded from, its raw essence; 'well', the source from which creation is drawn, that infinite pool of potential; and, in the very literal sense, I do a lot of writing, so it, also, works on that level.

## The Man

One can call me Blæcwell (Blacwell)—or Alder, if one wishes to be familiar. As of the writing of this, I am the lone man on this venture, but I hope to see that change in the future. I do not have any particular experience in this form of work, but I thought it important enough, and myself capable enought, to give it a good go. If one wishes to better know me, I imagine that looking to the work I have produced would give the best idea, but for those who want a little more, I shall share part of my, personal, motivation.

Since childhood, growing up in an English city, I've felt a hollowness from my environment; the world around me was a once beautiful seashell, washed onto the shore, now faded and broken, but a glimmer of its once great marvel remained. I am reminded of the Cathedral in that city: a most magnificent build, it is the dwells from a people who were imbued with a quality that is absent in today's world. While young, I was ignorant of that which was missing, but, now, I have come to a better understanding: it is the soul, that once bright hearth, that has gone dim.

That is why I am driven to this, to try and kindle that once mighty flame. And, I know, this sorrow is not just my own, many in Europe of a likemind. That is why my focus expand further than just England.

## The Fellowship

A foundational aspect of Blacwell is a focus on fellowship. I am never going to be the most knowledgeable, or skilled, man in any particular area; I am a broad base, which will better allow me to knit disparate elements together, but I shan't be as value in any specific field.

Not only for creation, but the benefits of the coming together of like-minded men—especially in time, such as these, where men of tradition and beset by many fiends—is valuable to a man's desire for kinship.
