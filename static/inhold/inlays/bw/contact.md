---
title: Contact Us
author: Alder Blæcwell
lead: "If you have business proposal, or would like to say 'hello', feel free to send me an email. If you are looking for a conversation, or to talk to others in the fellowship, head to our matrix server."
uttered: 1649437200000
edited:
tags:
  - blacwell
---

Personal Email: alder@blacwell.com
Business Email: business@blacwell.com
