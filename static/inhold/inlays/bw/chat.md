---
title: Join the Conversation
author: Alder Blæcwell
lead: 'We use a service called [matrix] to facilitate out online conversations (similar to Discord, if you are familiar). Click the below button to join.'
uttered: 1649437200000
edited:
tags:
  - blacwell
---

A little guidance?

1. After click the button, a new browser tab should open. If you have not used the service before, a page asking if you want to preview the link will apear; click the blue 'Continue' button.
2. A page should appear, asking you to 'Choose an app to continue'. The first box should say 'Element', and contain another blue 'Continue' button; click that.
3. You can either download the program, or view it in your browser.

<a href="https://matrix.to/#/#blacwell-chat:matrix.org" target="_blank">Join the conversation</a>
<a href="https://matrix.to/#/#blacwell-folkstead:matrix.org" target="_blank">Preview the public chat</a>
