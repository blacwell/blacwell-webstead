/// <reference types="@sveltejs/kit" />

declare namespace App {
	// interface Locals {}
	// interface Platform {}
	// interface Session {}
	interface Stuff {
		href: string;
	}
	interface Version {
		name?: string;
		pollInterval?: number;
	}
}
