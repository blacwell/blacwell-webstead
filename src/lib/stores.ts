import { browser } from '$app/env';
import { writable } from 'svelte/store';

function bake(name: string, fallback?: string) {
	if (!browser) return;
	const local = localStorage.getItem(name);
	const store = local ? writable(local) : writable(fallback);
	store.subscribe((value) => localStorage.setItem(name, value));
	return store;
}

export const showOverleaf = bake('showOverleaf', 'true');
export const theme = bake('theme', 'light');
