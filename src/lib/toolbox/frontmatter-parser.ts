export default function parser(markdown: string) {
	if (typeof markdown !== 'string')
		throw TypeError(`Frontmatter Parser: input requires string but got ${typeof markdown}`);
	// Checks to see if first 3 character matches '---'
	if (markdown.startsWith('---')) {
		// index after second instances of '---'
		const breakpoint = markdown.indexOf('---', 2);

		const head = markdown.slice(0, breakpoint);
		const body = markdown.slice(breakpoint + 3);

		return { head, body };
	} else {
		throw Error(`Frontmatter Parser: markdown does not contain frontmatter`);
	}
}
