import yaml from 'js-yaml';
import { marked } from 'marked';
import DOMPurify from 'dompurify';
import { browser } from '$app/env';
import fmParser from './frontmatter-parser';

function cleanHtmlFromString(data: unknown) {
	if (typeof data !== 'string')
		throw TypeError(`Wrong type: expected string but got ${typeof data}`);
	const dirty = marked.parse(data);
	const clean = browser ? DOMPurify.sanitize(dirty) : dirty;
	return clean;
}

export default function markdownReader(md: string) {
	const parsedData = fmParser(md);
	try {
		const attributes = yaml.load(parsedData.head);
		if (typeof attributes !== 'object')
			throw `js-yaml load function returned wrong type: ${typeof attributes}`;
		const body = cleanHtmlFromString(parsedData.body);
		return {
			attributes,
			body
		};
	} catch (error) {
		TypeError(error);
	}
}
