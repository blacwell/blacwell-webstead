import { assets } from '$app/paths';
import markdownReader from './markdown-reader';

async function inlayRequest(url: string, fetch) {
	// Add leading '/'
	const path = url.charAt(0) === '/' ? url : `/${url}`;
	try {
		const response = await fetch(`${assets}/inhold/inlays${path}.md`);
		if (response.status === 404) throw `A file was not found.`;
		const data = await response.text();
		const organizedData = markdownReader(data);

		const attributes = {
			...organizedData.attributes,
			uttered: new Date(organizedData.attributes['uttered']),
			url: path
		};
		const body = organizedData.body;

		return { attributes, body };
	} catch (error) {
		return Error(error);
	}
}

// For fetching a single inlay
export async function fetchInlay(params: Record<string, string>, fetch) {
	const path = Object.values(params).join('/');

	try {
		return await inlayRequest(path, fetch);
	} catch (error) {
		return { status: 404, error };
	}
}

// For fetching multiple inlays
export async function fetchInlays(fetch) {
	try {
		const response = await fetch(`${assets}/inhold/inlays/contents.json`);
		if (response.status === 404) throw new Error(`Contents file was not found.`);

		const contentsData = await response.json();
		const data = [];

		for (const inlays in contentsData) {
			if (Object.prototype.hasOwnProperty.call(contentsData, inlays)) {
				const inlay = contentsData[inlays];
				const path = `${inlay.kind}/${inlay.name}`;
				data.push(await inlayRequest(path, fetch));
			}
		}
		return data;
	} catch (error) {
		return { status: 404, error };
	}
}
