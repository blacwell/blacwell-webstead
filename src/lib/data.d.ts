interface Inlay {
	attributes: {
		title: string;
		author: string;
		lead: string;
		thumbnail?;
		uttered: Date;
		edited: Date | undefined;
		audio?: string;
		tags: string[];
		url?: string;
	};
	body: string;
}

type Inlays = Inlay[];
